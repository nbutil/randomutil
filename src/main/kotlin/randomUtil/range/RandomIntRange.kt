package randomUtil.range

import randomUtil.BaseRandom

/**
 * A Range of [Int] that produces random values from within the given range.
 *
 * @property[minimum] The minimum amount in the range
 * @property[maximum] The maximum amount on the range
 * @property[inclusive] Whether or not to include [maximum] in the range
 */
class RandomIntRange(minimum: Int, maximum: Int, private val inclusive: Boolean, seed: Long? = null): BaseRandom<Int>(seed) {

    val minimum = minOf(minimum, maximum)

    val maximum = maxOf(minimum, maximum)

    override fun copyOf(): RandomIntRange = RandomIntRange(minimum = minimum, maximum = maximum, inclusive = inclusive, seed = initialSeed)

    override fun nextValue(modifier: Int): Int = this.nextInt(minimum, maximum, inclusive)

    override fun toString(): String = "Int[$minimum -> $maximum${if (inclusive) "]" else ")"}"

    override fun hashCode(): Int {
        var hash = 19 * (31*31*31)
        hash += minimum.hashCode() * (31*31)
        hash += maximum.hashCode() * 31
        return hash + inclusive.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is RandomIntRange) return false
        if (other === this) return true
        return this.minimum == other.minimum && this.maximum == other.maximum && this.inclusive == other.inclusive
    }

}
