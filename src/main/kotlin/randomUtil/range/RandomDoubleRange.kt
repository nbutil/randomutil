package randomUtil.range

import randomUtil.BaseRandom

/**
 * A Range of [Double] that produces random values from within the given range.
 *
 * @property[minimum] The minimum amount in the range
 * @property[maximum] The maximum amount on the range
 * @property[inclusive] Whether or not to include [maximum] in the range
 */
class RandomDoubleRange(minimum: Double, maximum: Double, private val inclusive: Boolean, seed: Long? = null): BaseRandom<Double>(seed) {

    val minimum = minOf(minimum, maximum)

    val maximum = maxOf(minimum, maximum)

    override fun copyOf(): RandomDoubleRange = RandomDoubleRange(minimum = minimum, maximum = maximum, inclusive = inclusive, seed = initialSeed)

    override fun nextValue(modifier: Int): Double = this.nextDouble(minimum, maximum)

    override fun toString(): String = "Double[$minimum -> $maximum$)"

    override fun hashCode(): Int {
        var hash = 31 * (31*31*31)
        hash += minimum.hashCode() * (31*31)
        hash += maximum.hashCode() * 31
        return hash + inclusive.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is RandomDoubleRange) return false
        if (other === this) return true
        return this.minimum == other.minimum && this.maximum == other.maximum && this.inclusive == other.inclusive
    }

}
