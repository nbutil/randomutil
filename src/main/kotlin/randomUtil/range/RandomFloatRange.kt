package randomUtil.range

import randomUtil.BaseRandom

/**
 * A Range of [Float] that produces random values from within the given range.
 *
 * @property[minimum] The minimum amount in the range
 * @property[maximum] The maximum amount on the range
 * @property[inclusive] Whether or not to include [maximum] in the range
 */
class RandomFloatRange(minimum: Float, maximum: Float, private val inclusive: Boolean, seed: Long? = null): BaseRandom<Float>(seed) {

    val minimum = minOf(minimum, maximum)

    val maximum = maxOf(minimum, maximum)

    override fun copyOf(): RandomFloatRange = RandomFloatRange(minimum = minimum, maximum = maximum, inclusive = inclusive, seed = initialSeed)

    override fun nextValue(modifier: Int): Float = this.nextFloat(minimum, maximum)

    override fun toString(): String = "Float[$minimum -> $maximum$)"

    override fun hashCode(): Int {
        var hash = 23 * (31*31*31)
        hash += minimum.hashCode() * (31*31)
        hash += maximum.hashCode() * 31
        return hash + inclusive.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is RandomFloatRange) return false
        if (other === this) return true
        return this.minimum == other.minimum && this.maximum == other.maximum && this.inclusive == other.inclusive
    }

}
