package randomUtil.shuffle

import kotlin.random.Random

/**
 * Shuffles the [Array] in place.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Unit]
 */
@JvmOverloads fun <T> Array<T>.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: T
    for (currentIndex in 0 until this.size) {
        nextIndex = withRandom.nextInt(size)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [Array] but shuffled. Doesn't modify the original array.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Array]
 */
@JvmOverloads fun <T> Array<T>.shuffled(withRandom: Random = Random): Array<T> {
    val toReturn = this.copyOf()
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}

/**
 * Shuffles the [BooleanArray] in place.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Unit]
 */
@JvmOverloads fun BooleanArray.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Boolean
    for (currentIndex in 0 until this.size) {
        nextIndex = withRandom.nextInt(size)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [BooleanArray] but shuffled. Doesn't modify the original array.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [BooleanArray]
 */
@JvmOverloads fun BooleanArray.shuffled(withRandom: Random = Random): BooleanArray {
    val toReturn = this.copyOf()
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}

/**
 * Shuffles the [ByteArray] in place.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Unit]
 */
@JvmOverloads fun ByteArray.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Byte
    for (currentIndex in 0 until this.size) {
        nextIndex = withRandom.nextInt(size)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [ByteArray] but shuffled. Doesn't modify the original array.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [ByteArray]
 */
@JvmOverloads fun ByteArray.shuffled(withRandom: Random = Random): ByteArray {
    val toReturn = this.copyOf()
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}

/**
 * Shuffles the [ShortArray] in place.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Unit]
 */
@JvmOverloads fun ShortArray.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Short
    for (currentIndex in 0 until this.size) {
        nextIndex = withRandom.nextInt(size)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [ShortArray] but shuffled. Doesn't modify the original array.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [ShortArray]
 */
@JvmOverloads fun ShortArray.shuffled(withRandom: Random = Random): ShortArray {
    val toReturn = this.copyOf()
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}

/**
 * Shuffles the [CharArray] in place.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Unit]
 */
@JvmOverloads fun CharArray.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Char
    for (currentIndex in 0 until this.size) {
        nextIndex = withRandom.nextInt(size)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [CharArray] but shuffled. Doesn't modify the original array.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [CharArray]
 */
@JvmOverloads fun CharArray.shuffled(withRandom: Random = Random): CharArray {
    val toReturn = this.copyOf()
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}

/**
 * Shuffles the [IntArray] in place.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Unit]
 */
@JvmOverloads fun IntArray.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Int
    for (currentIndex in 0 until this.size) {
        nextIndex = withRandom.nextInt(size)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [IntArray] but shuffled. Doesn't modify the original array.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [IntArray]
 */
@JvmOverloads fun IntArray.shuffled(withRandom: Random = Random): IntArray {
    val toReturn = this.copyOf()
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}

/**
 * Shuffles the [FloatArray] in place.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Unit]
 */
@JvmOverloads fun FloatArray.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Float
    for (currentIndex in 0 until this.size) {
        nextIndex = withRandom.nextInt(size)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [FloatArray] but shuffled. Doesn't modify the original array.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [FloatArray]
 */
@JvmOverloads fun FloatArray.shuffled(withRandom: Random = Random): FloatArray {
    val toReturn = this.copyOf()
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}

/**
 * Shuffles the [LongArray] in place.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Unit]
 */
@JvmOverloads fun LongArray.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Long
    for (currentIndex in 0 until this.size) {
        nextIndex = withRandom.nextInt(size)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [LongArray] but shuffled. Doesn't modify the original array.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [LongArray]
 */
@JvmOverloads fun LongArray.shuffled(withRandom: Random = Random): LongArray {
    val toReturn = this.copyOf()
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}

/**
 * Shuffles the [DoubleArray] in place.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [Unit]
 */
@JvmOverloads fun DoubleArray.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Double
    for (currentIndex in 0 until this.size) {
        nextIndex = withRandom.nextInt(size)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [DoubleArray] but shuffled. Doesn't modify the original array.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [DoubleArray]
 */
@JvmOverloads fun DoubleArray.shuffled(withRandom: Random = Random): DoubleArray {
    val toReturn = this.copyOf()
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}
