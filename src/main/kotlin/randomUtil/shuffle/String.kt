package randomUtil.shuffle

import kotlin.random.Random

/**
 * Returns a shuffled version of the given [String]
 *
 * @return [String]
 */
@JvmOverloads fun String.shuffled(withRandom: Random = Random): String = this.toCharArray().shuffled(withRandom).joinToString("")

/**
 * Shuffles the [StringBuilder] in place
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return Unit
 */
@JvmOverloads fun StringBuilder.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Char
    for (currentIndex in 0 until this.length) {
        nextIndex = withRandom.nextInt(this.length)
        temp = this[currentIndex]
        this[currentIndex] = this[nextIndex]
        this[nextIndex] = temp
    }
}

/**
 * Returns a copy of the [StringBuilder] but shuffled. Doesn't modify the original.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [StringBuilder]
 */
@JvmOverloads fun StringBuilder.shuffled(withRandom: Random = Random): StringBuilder {
    val toReturn = StringBuilder(this)
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}

/**
 * Shuffles the [StringBuilder] in place
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return Unit
 */
@JvmOverloads fun StringBuffer.shuffleInPlace(withRandom: Random = Random) {
    var nextIndex: Int
    var temp: Char
    for (currentIndex in 0 until this.length) {
        nextIndex = withRandom.nextInt(this.length)
        temp = this[currentIndex]
        this.setCharAt(currentIndex, this[nextIndex])
        this.setCharAt(nextIndex, temp)
    }
}

/**
 * Returns a copy of the [StringBuilder] but shuffled. Doesn't modify the original.
 *
 * @param [withRandom] The [Random] to use while shuffling
 * @return [StringBuilder]
 */
@JvmOverloads fun StringBuffer.shuffled(withRandom: Random = Random): StringBuffer {
    val toReturn = StringBuffer(this)
    toReturn.shuffleInPlace(withRandom)
    return toReturn
}