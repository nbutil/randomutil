package randomUtil.progression

import randomUtil.BaseRandom
import randomUtil.range.RandomLongRange
import kotlin.math.absoluteValue
import kotlin.math.floor

/**
 * Generates a [Long] in uniform from [first] to [last] with a gap of [step] between each generated value.
 *
 * @property [first] The first value in the [RandomLongProgression]
 * @property [last] The last value in the [RandomLongProgression]
 * @property [step] The delta between a given value and the possible next value in the progression
 * @property [base] The base value (also the first value)
 * @property [scalar] The scalar range that determines actual [step]
 * @constructor Creates a new [RandomLongProgression] from [first] [last] and [step].
 */
class RandomLongProgression(first: Long, last: Long, step: Long = 1, seed: Long? = null): BaseRandom<Long>(seed) {
    val first: Long
    val last: Long
    val step: Long
    private val base: Long
    private val scalar: RandomLongRange

    init {
        if (step <= 0) {
            throw InvalidStep("Invalid step [$step]: must be greater than 0")
        }
        this.base = first
        if (first < last) {
            this.first = first
            this.last = (first .. last step step).last
            this.step = step
            this.scalar = RandomLongRange(0, (floor(((last - first) + step).toDouble()) / step).toLong(), false)
        } else {
            this.first = last
            this.last = (last .. first step step).last
            this.step = (-step)
            this.scalar = RandomLongRange(0, floor(((first - last) - step).toDouble() / step).absoluteValue.toLong(), false)
        }
    }

    override fun copyOf(): RandomLongProgression = RandomLongProgression(first = first, last = last, step = step, seed = initialSeed)

    override fun nextValue(modifier: Int): Long = (this.base + (this.scalar.nextValue() * this.step))

    override fun hashCode(): Int {
        var hash = 3 * (31 * 31 * 31)
        hash += first.hashCode() * (31 * 31)
        hash += last.hashCode() * 31
        return hash + step.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is RandomLongProgression ) return false
        if (other === this) return true
        return this.first == other.first && this.last == other.last && this.step == other.step
    }
}