package randomUtil.progression

import randomUtil.BaseRandom
import randomUtil.range.RandomByteRange
import kotlin.math.absoluteValue
import kotlin.math.floor

/**
 * Generates a [Byte] in uniform from [first] to [last] with a gap of [step] between each generated value.
 *
 * @property [first] The first value in the [RandomByteProgression]
 * @property [last] The last value in the [RandomByteProgression]
 * @property [step] The delta between a given value and the possible next value in the progression
 * @property [base] The base value (also the first value)
 * @property [scalar] The scalar range that determines actual [step]
 * @constructor Creates a new [RandomByteProgression] from [first] [last] and [step].
 */
class RandomByteProgression(first: Byte, last: Byte, step: Byte = 1, seed: Long? = null): BaseRandom<Byte>(seed) {
    val first: Byte
    val last: Byte
    val step: Byte
    private val base: Byte
    private val scalar: RandomByteRange

    init {
        if (step <= 0) {
            throw InvalidStep("Invalid step [$step]: must be greater than 0")
        }
        this.base = first
        if (first < last) {
            this.first = first
            this.last = (first .. last step step.toInt()).last.toByte()
            this.step = step
            this.scalar = RandomByteRange(0, (floor(((last - first) + step).toDouble()) / step).toByte(), false)
        } else {
            this.first = last
            this.last = (last .. first step step.toInt()).last.toByte()
            this.step = (-step).toByte()
            this.scalar = RandomByteRange(0, floor(((first - last) - step).toDouble() / step).absoluteValue.toByte(), false)
        }
    }

    override fun copyOf(): RandomByteProgression = RandomByteProgression(first = first, last = last, step = step, seed = initialSeed)

    override fun nextValue(modifier: Int): Byte = (this.base + (this.scalar.nextValue() * this.step)).toByte()

    override fun hashCode(): Int {
        var hash = 3 * (31 * 31 * 31)
        hash += first.hashCode() * (31 * 31)
        hash += last.hashCode() * 31
        return hash + step.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is RandomByteProgression ) return false
        if (other === this) return true
        return this.first == other.first && this.last == other.last && this.step == other.step
    }
}