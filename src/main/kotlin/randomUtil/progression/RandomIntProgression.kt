package randomUtil.progression

import randomUtil.BaseRandom
import randomUtil.range.RandomIntRange
import kotlin.math.absoluteValue
import kotlin.math.floor

/**
 * Generates a [Int] in uniform from [first] to [last] with a gap of [step] between each generated value.
 *
 * @property [first] The first value in the [RandomIntProgression]
 * @property [last] The last value in the [RandomIntProgression]
 * @property [step] The delta between a given value and the possible next value in the progression
 * @property [base] The base value (also the first value)
 * @property [scalar] The scalar range that determines actual [step]
 * @constructor Creates a new [RandomIntProgression] from [first] [last] and [step].
 */
class RandomIntProgression(first: Int, last: Int, step: Int = 1, seed: Long? = null): BaseRandom<Int>(seed) {
    val first: Int
    val last: Int
    val step: Int
    private val base: Int
    private val scalar: RandomIntRange

    init {
        if (step <= 0) {
            throw InvalidStep("Invalid step [$step]: must be greater than 0")
        }
        this.base = first
        if (first < last) {
            this.first = first
            this.last = (first .. last step step).last
            this.step = step
            this.scalar = RandomIntRange(0, (floor(((last - first) + step).toDouble()) / step).toInt(), false)
        } else {
            this.first = last
            this.last = (last .. first step step).last
            this.step = (-step)
            this.scalar = RandomIntRange(0, floor(((first - last) - step).toDouble() / step).absoluteValue.toInt(), false)
        }
    }

    override fun copyOf(): RandomIntProgression = RandomIntProgression(first = first, last = last, step = step, seed = initialSeed)

    override fun nextValue(modifier: Int): Int = (this.base + (this.scalar.nextValue() * this.step))

    override fun hashCode(): Int {
        var hash = 3 * (31 * 31 * 31)
        hash += first.hashCode() * (31 * 31)
        hash += last.hashCode() * 31
        return hash + step.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is RandomIntProgression ) return false
        if (other === this) return true
        return this.first == other.first && this.last == other.last && this.step == other.step
    }
}