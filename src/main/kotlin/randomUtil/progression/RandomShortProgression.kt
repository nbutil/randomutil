package randomUtil.progression

import randomUtil.BaseRandom
import randomUtil.range.RandomShortRange
import kotlin.math.absoluteValue
import kotlin.math.floor

/**
 * Generates a [Short] in uniform from [first] to [last] with a gap of [step] between each generated value.
 *
 * @property [first] The first value in the [RandomShortProgression]
 * @property [last] The last value in the [RandomShortProgression]
 * @property [step] The delta between a given value and the possible next value in the progression
 * @property [base] The base value (also the first value)
 * @property [scalar] The scalar range that determines actual [step]
 * @constructor Creates a new [RandomShortProgression] from [first] [last] and [step].
 */
class RandomShortProgression(first: Short, last: Short, step: Short = 1, seed: Long? = null): BaseRandom<Short>(seed) {
    val first: Short
    val last: Short
    val step: Short
    private val base: Short
    private val scalar: RandomShortRange

    init {
        if (step <= 0) {
            throw InvalidStep("Invalid step [$step]: must be greater than 0")
        }
        this.base = first
        if (first < last) {
            this.first = first
            this.last = (first .. last step step.toInt()).last.toShort()
            this.step = step
            this.scalar = RandomShortRange(0, (floor(((last - first) + step).toDouble()) / step).toShort(), false)
        } else {
            this.first = last
            this.last = (last .. first step step.toInt()).last.toShort()
            this.step = (-step).toShort()
            this.scalar = RandomShortRange(0, floor(((first - last) - step).toDouble() / step).absoluteValue.toShort(), false)
        }
    }

    override fun copyOf(): RandomShortProgression = RandomShortProgression(first = first, last = last, step = step, seed = initialSeed)

    override fun nextValue(modifier: Int): Short = (this.base + (this.scalar.nextValue() * this.step)).toShort()

    override fun hashCode(): Int {
        var hash = 3 * (31 * 31 * 31)
        hash += first.hashCode() * (31 * 31)
        hash += last.hashCode() * 31
        return hash + step.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is RandomShortProgression ) return false
        if (other === this) return true
        return this.first == other.first && this.last == other.last && this.step == other.step
    }
}