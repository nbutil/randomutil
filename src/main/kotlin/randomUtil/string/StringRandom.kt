package randomUtil.string

import randomUtil.BaseRandom

/**
 * Generates random strings from a given [sourceString]
 *
 * @property [sourceString] The source of this [StringRandom] output
 * @property [defaultRandomLength] The default output length when using [nextValue] with no parameters
 * @constructor Creates a [StringRandom] with the source of output as [sourceString]
 */
class StringRandom(
    private val sourceString: String,
    private val defaultRandomLength: Int = sourceString.length, // I love how kotlin lets you do this in constructors
    seed: Long? = null): BaseRandom<String>(seed) {
    /**
     * Generates a [String] of default length [defaultRandomLength]
     *
     * @return [String]
     */
    override fun nextValue(): String = nextValue(defaultRandomLength)

    /**
     * Generates a [modifier] length [String]
     *
     * @param [modifier] The length of the [String] to return
     * @return [String]
     */
    override fun nextValue(modifier: Int): String {
        var finalString = ""
        repeat(modifier) {
            finalString += sourceString.random(internalRandom)
        }
        return finalString
    }

    override fun equals(other: Any?): Boolean {
        if (other !is StringRandom) return false
        if (other === this) return true
        return this.sourceString == other.sourceString
    }

    override fun hashCode(): Int {
        var hash = 43 * (31 * 31)
        hash += sourceString.hashCode() * 31
        return hash + defaultRandomLength.hashCode()
    }

    override fun copyOf(): StringRandom = StringRandom(sourceString = sourceString, seed = initialSeed)
}
