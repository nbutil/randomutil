package randomUtil

/** Classes and objects that implement this interface MUST define a [hashCode] function and an [equals] function */
internal interface MustDefineHashAndEquals {
    override fun hashCode(): Int
    override fun equals(other: Any?): Boolean
}