package randomUtil.universal

/**
 * An [Exception] thrown when the [UniversalRandom] is empty
 */
class EmptySources(override val message: String): Exception()

/**
 * An [Exception] thrown when the [UniversalRandom] has no sources found for the given type
 */
class SourceNotFound(override val message: String) : Exception()

/**
 * An [Exception] thrown when a source for the given type already exists
 */
class SourceAlreadyExists(override val message: String) : Exception()