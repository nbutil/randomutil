package randomUtil.universal

import randomUtil.BaseRandom
import kotlin.reflect.KClass

/**
 * Holds [BaseRandom] sources for any type. Can only have 1 of each type.
 *
 * @constructor Creates a [UniversalRandom] object with nothing in it.
 */
class UniversalRandom(seed: Long? = null) : BaseRandom<Any>(seed) {

    /** @property[randomSources] The random sources to pick from */
    private val randomSources: HashMap<KClass<*>, BaseRandom<*>> = hashMapOf()

    /**
     * Adds a source to [randomSources]
     *
     * @param[type] The return type of the source being added
     * @param[source] The random source to generate a [type] from
     * @param[overwrite] Whether or not to overwrite an existing [type] source
     */
    fun <T: Any> addSource(type: KClass<Any>, source: BaseRandom<T>, overwrite: Boolean=false) {
        if (!type.isInstance(source.nextValue())) {
            throw IllegalAccessError("Cannot assign type ${type.qualifiedName} to return source type source $source")
        }
        if (type in randomSources.keys && !overwrite) {
            throw SourceAlreadyExists("A source already exists for ${type.qualifiedName}")
        }
        randomSources[type] = source
    }
    inline fun <reified T: Any> addSource(source: BaseRandom<T>, overwrite: Boolean=false) = addSource(T::class as KClass<Any>, source, overwrite)

    /**
     * Checks if this [UniversalRandom] is empty.
     *
     * @param[throwException] Whether or not to throw an exception if the random is empty.
     *
     * @return [Boolean]
     * @throws [EmptySources]
     */
    @JvmOverloads fun isEmpty(throwException: Boolean=false): Boolean {
        if (randomSources.isEmpty()) {
            if (throwException) {
                throw EmptySources("No random sources given.")
            }
            return true
        }
        return false
    }

    /**
     * Gets the next value of [type]
     *
     * @param[type] The type of the value to return.
     *
     * @return [T]
     * @throws [SourceNotFound]
     */
    fun <T: Any> nextValueOf(type: KClass<T>): T {
        isEmpty(true)
        if (type !in randomSources.keys) {
            throw SourceNotFound("Source not found for type: ${type.qualifiedName}")
        }
        return randomSources[type].castedTo<T>().nextValue()
    }

    /**
     * Gets the next value of [T]
     *
     * @return [T]
     * @throws [SourceNotFound]
     */
    inline fun <reified T: Any> nextValueOf(): T = nextValueOf(T::class)

    private fun <T> BaseRandom<*>?.castedTo() = this as BaseRandom<T>

    override fun nextValue(modifier: Int): Any {
        isEmpty(true)
        return randomSources[randomSources.keys.random(internalRandom)].castedTo<Any>().nextValue()
    }

    override fun copyOf(): UniversalRandom {
        val returnUniversalRandom = UniversalRandom(initialSeed)
        for (type in randomSources.keys) {
            returnUniversalRandom.addSource(type as KClass<Any>, randomSources[type].castedTo<Any>().copyOf(), true)
        }
        return returnUniversalRandom
    }

    override fun equals(other: Any?): Boolean {
        if (other !is UniversalRandom) return false
        if (other === this) return true
        return this.randomSources.hashCode() == other.randomSources.hashCode()
    }

    override fun hashCode(): Int = (37 * 31) + randomSources.hashCode()

}
