package randomUtil.universal

import randomUtil.BaseRandom

/**
 * Creates a [UniversalRandom] with the given [randoms] as sources
 *
 * @param[randoms] The random sources to put in the [UniversalRandom]
 *
 * @return [UniversalRandom]
 */
inline fun <reified T: Any> univeralRandomOf(vararg randoms: BaseRandom<T>): UniversalRandom =
    UniversalRandom()
        .apply { randoms.forEach { addSource(it) } }

/**
 * Creates a [UniversalRandom] with the given [randoms] as sources and the given [seed]
 *
 * @param[seed] The seed to initialize the [BaseRandom] with
 * @param[randoms] The random sources to put in the [UniversalRandom]
 *
 * @return [UniversalRandom]
 */
inline fun <reified T: Any> universalRandomOf(seed: Long, vararg randoms: BaseRandom<T>): UniversalRandom =
    UniversalRandom(seed)
        .apply { randoms.forEach { addSource(it) } }
