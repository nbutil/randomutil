package randomUtil.number

import randomUtil.BaseRandom

/**
 * A [BaseRandom] class that generates numbers.
 * @see [BaseRandom] for function docs, as this is just a wrapper that makes them public.
 *
 * @param [seed] The seed to initialize the [BaseRandom] with
 */
class NumberRandom(seed: Long? = null): BaseRandom<Number>(seed) {
    fun nextByteValue(lowerBound: Byte, upperBound: Byte, inclusive: Boolean): Byte =
        nextByte(lowerBound, upperBound, inclusive)

    fun nextShortValue(lowerBound: Short, upperBound: Short, inclusive: Boolean): Short =
        nextShort(lowerBound, upperBound, inclusive)

    fun nextIntValue(lowerBound: Int, upperBound: Int, inclusive: Boolean): Int =
        nextInt(lowerBound, upperBound, inclusive)

    fun nextLongValue(lowerBound: Long, upperBound: Long, inclusive: Boolean): Long =
        nextLong(lowerBound, upperBound, inclusive)

    fun nextFloatValue(lowerBound: Float, upperBound: Float): Float =
        nextFloat(lowerBound, upperBound)

    fun nextDoubleValue(lowerBound: Double, upperBound: Double): Double =
        nextDouble(lowerBound, upperBound)

    override fun nextValue(): Number = nextInt(Int.MIN_VALUE, Int.MAX_VALUE, true)
    override fun nextValue(modifier: Int): Number = nextValue()

    override fun copyOf(): NumberRandom = NumberRandom(initialSeed)
    override fun hashCode(): Int = (initialSeed.hashCode() * 31)
    override fun equals(other: Any?): Boolean {
        if (other !is NumberRandom) return false
        if (other === this) return true
        return this.hashCode() == other.hashCode()
    }
}