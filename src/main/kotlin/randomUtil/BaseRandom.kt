package randomUtil

import kotlin.random.Random

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * The Base class for this project.
 * None of the protected functions are meant to be exposed for the purpose of possibly random generators or random
 * collections. Why would something that generates a List have a nextFloat or nextByte function?
 * All functions inside are just [Random.nextInt] or [Random.nextDouble] with the result converted to the return type.
 */
abstract class BaseRandom<T>(seed: Long?): MustDefineHashAndEquals {
    private class InclusiveOverflow(override val message: String): Exception()

    protected val initialSeed: Long = seed ?: System.currentTimeMillis()

    protected val internalRandom: Random = Random(initialSeed)

    /**
     * Generates a [Byte] within the range [lowerBound] and [upperBound] with [inclusive] defining inclusivity
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value (depending on [inclusive]) to generate
     * @param [inclusive] Whether or not to include the actual [upperBound]
     * @return [Byte]
     */
    protected fun nextByte(lowerBound: Byte, upperBound: Byte, inclusive: Boolean): Byte =
        internalRandom.nextInt(lowerBound.toInt(), (upperBound + (if (inclusive) 1 else 0))).toByte()

    /**
     * Generates a [Short] within the range [lowerBound] and [upperBound] with [inclusive] defining inclusivity
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value (depending on [inclusive]) to generate
     * @param [inclusive] Whether or not to include the actual [upperBound]
     * @return [Short]
     */
    protected fun nextShort(lowerBound: Short, upperBound: Short, inclusive: Boolean): Short =
        internalRandom.nextInt(lowerBound.toInt(), (upperBound + (if (inclusive) 1 else 0))).toShort()

    /**
     * Generates an [Int] within the range [lowerBound] and [upperBound] with [inclusive] defining inclusivity
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value (depending on [inclusive]) to generate
     * @param [inclusive] Whether or not to include the actual [upperBound]
     * @return [Int]
     */
    protected fun nextInt(lowerBound: Int, upperBound: Int, inclusive: Boolean): Int =
        internalRandom.nextLong(lowerBound.toLong(), upperBound + (if (inclusive) 1 else 0).toLong()).toInt()

    /**
     * Generates a [Long] within the range [lowerBound] and [upperBound] with [inclusive] defining inclusivity
     * Can only be [inclusive] if the delta between [lowerBound] and [upperBound] is less than [ULong.MAX_VALUE] due to overflow problems
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value (depending on [inclusive]) to generate
     * @param [inclusive] Whether or not to include the actual [upperBound]
     * @return [Long]
     */
    protected fun nextLong(lowerBound: Long, upperBound: Long, inclusive: Boolean = false): Long {
        if (inclusive) {
            if (upperBound == Long.MAX_VALUE && lowerBound == Long.MIN_VALUE) {
                throw InclusiveOverflow("Cannot be inclusive when range is ${Long.MIN_VALUE .. Long.MAX_VALUE} due to overflow")
            }
            if (upperBound == Long.MAX_VALUE) {
                return internalRandom.nextLong(lowerBound-1, upperBound) + 1
            }
            return internalRandom.nextLong(lowerBound, upperBound + 1)
        }
        return internalRandom.nextLong(lowerBound, upperBound)
    }

    /**
     * Generates a [Float] within the range [lowerBound] and [upperBound]
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value
     * @return [Float]
     */
    protected fun nextFloat(lowerBound: Float, upperBound: Float): Float =
        internalRandom.nextDouble(lowerBound.toDouble(), upperBound.toDouble()).toFloat()

    /**
     * Generates a [Double] within the range [lowerBound] and [upperBound]
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value
     * @return [Double]
     */
    protected fun nextDouble(lowerBound: Double, upperBound: Double): Double =
        internalRandom.nextDouble(lowerBound, upperBound)

    /**
     * Generates a new value of type [T] based on modifier [modifier]
     *
     * @return [T]
     */
    abstract fun nextValue(modifier: Int): T

    /**
     * Generates a new value of type [T] with the default modifier 0
     *
     * @return [T]
     */
    open fun nextValue(): T = nextValue(0)

    /**
     * Generates a [Map]<[T], [Int]> by calling [nextValue] [amount] times with the given modifier [modifier]
     *
     * @param[amount] The amount of times to call [nextValue]
     * @param[modifier] The modifier to pass in to [nextValue]
     * @return [Map]<[T], [Int]>
     */
    open fun nextValueMap(amount: Int, modifier: Int): Map<T, Int> =
        HashMap<T, Int>().apply {
            repeat(amount) {
                increment(nextValue(modifier))
            }
        }

    /**
     * Generates a [Map]<[T]> by calling [nextValue] [amount] times with the default modifier 0
     * The keys will be the items and the values will be the amount of times the item was picked
     *
     * @param[amount] The amount of times to call [nextValue].
     * @return [Map]<[T], [Int]>
     */
    open fun nextValueMap(amount: Int): Map<T, Int> = nextValueMap(amount, 0)

    /**
     * Generates a [List]<[T]> of size [amount] by calling [nextValue]
     *
     * @param[amount] The size of the [List]
     * @param[modifier] The modifier to pass in to [nextValue]
     * @return [List]<[T]>
     */
    open fun nextValueList(amount: Int, modifier: Int): List<T> =
        List(amount) { nextValue(modifier) }

    /**
     * Generates a [List]<[T]> of size [amount] by calling [nextValue]
     *
     * @param[amount] The size of the [List]
     * @param[modifier] The modifier to pass in to [nextValue]
     * @return [List]<[T]>
     */
    open fun nextValueList(amount: Int): List<T> = nextValueList(amount, 0)


    /**
     * Creates a copy of this [BaseRandom]
     *
     * @return [BaseRandom]
     */
    abstract fun copyOf(): BaseRandom<T>
}
