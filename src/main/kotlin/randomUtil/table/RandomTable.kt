package randomUtil.table

import randomUtil.BaseRandom

/**
 * [RandomTable] by itself is essentially a [List.random] wrapper. No built in bias.
 *
 * @constructor Creates a [RandomTable] with the given [items]
 * @property [items] The items to initialize the [RandomTable] with
 */
open class RandomTable<T>(items: List<T>, seed: Long? = null): BaseRandom<T>(seed) {

    val items: List<T> = items.toList()

    /**
     * @constructor Creates a [RandomTable] with the given [items]
     * @param [items] A vararg parameter of [T] to initialize the [RandomTable] with
     */
    constructor(vararg items: T): this(items.toList())

    /**
     * Check if [other] is contained in [items]
     *
     * @param [other] The [T] to check is contained in [items]
     * @return [Boolean]
     */
    operator fun contains(other: T): Boolean = other in this.items

    /**
     * Gets a random value contained in [items]
     *
     * @param [modifier] unused
     * @return [T]
     */
    override fun nextValue(modifier: Int): T = items.random(internalRandom)

    /**
     * Creates a [RandomTable] copy of this
     *
     * @return [RandomTable]
     */
    fun toRandomTable(): RandomTable<T> = RandomTable(items)

    /**
     * Creates a [WeightedTable] copy of this with the given [weights]
     *
     * @param [weights] The weights to use for the [WeightedTable]
     * @return [WeightedTable]<[T]>
     */
    fun toWeightedTable(weights: IntArray): WeightedTable<T> = WeightedTable(items, weights, initialSeed)

    override fun copyOf(): RandomTable<T> = RandomTable(items, initialSeed)

    override fun hashCode(): Int = (47 * 31) + items.hashCode()

    override fun toString(): String = "RandomTable$items"

    override fun equals(other: Any?): Boolean {
        if (other !is RandomTable<*>) return false
        if (other === this) return true
        return this.items == other.items
    }
}
