package randomUtil.table

import randomUtil.range.RandomLongRange

/**
 * [MemoizedWeightedTable] is a memory-speed tradeoff compared to [WeightedTable] and is optimized for frequent use of other weights.
 * Weights that pass [assessWeightsAgainst] will have its [hashCode] assigned to a [Triple] consisting of the weight range, the [ListRemap]<[T]> of the sorted items and an [IntArray] of the sorted weights.
 * Weights that fail [assessWeightsAgainst] will have its [hashCode] assigned to a null.
 * Calling [getItemsFor], [getWeightsFor] and [getWeightRangeFor] on weights that failed [assessWeightsAgainst] will return [items], [weights] and [weightRange]
 *
 * @property [memoized] A [HashMap]<[Int], [Triple]<[RandomLongRange], [List]<[T]>, [IntArray]>?> that caches the results of any custom weights put in to it.
 * @constructor Creates a [MemoizedWeightedTable]
 */
open class MemoizedWeightedTable<T>(
    items: List<T>, weights: IntArray, seed: Long? = null,
    weightModifier: (Int) -> Double = { 1.0 / (1 + (it / 100.0)) }
): WeightedTable<T>(items, weights, seed, weightModifier) {

    protected val memoized: HashMap<Int, Triple<RandomLongRange, List<T>, IntArray>?> = hashMapOf()

    init { memoizeWeights(weights) }

    protected fun memoizeWeights(weightsToMemoize: IntArray) {
        if (weightsToMemoize.hashCode() !in memoized) {
            weightsToMemoize.assessWeightsAgainst(items)
                .doWhenInvalid {
                    memoized[weightsToMemoize.hashCode()] = null }
                .doWhenValid {
                    memoized[weightsToMemoize.hashCode()] =
                        Triple(
                            RandomLongRange(1L, weightsToMemoize.longSum(), true),
                            sortedListRemap(items, weightsToMemoize),
                            weightsToMemoize.sortedArray()
                        )
                }
        }
    }

    override fun getItemsFor(otherWeights: IntArray): List<T> =
        (if (memoized[otherWeights.hashCode()] == null)
            items
        else
            memoized[otherWeights.hashCode()]!!.second)

    override fun onOtherWeights(otherWeights: IntArray): Boolean {
        memoizeWeights(otherWeights)
        return memoized[otherWeights.hashCode()] == null
    }

    override fun getWeightRangeFor(otherWeights: IntArray): RandomLongRange =
        if (memoized[otherWeights.hashCode()] == null)
            weightRange
        else
            memoized[otherWeights.hashCode()]!!.first

    override fun getWeightsFor(otherWeights: IntArray): IntArray =
        if (memoized[otherWeights.hashCode()] == null)
            weights
        else
            memoized[otherWeights.hashCode()]!!.third
}
