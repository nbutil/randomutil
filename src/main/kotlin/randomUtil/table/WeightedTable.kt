package randomUtil.table

import randomUtil.increment
import randomUtil.range.RandomLongRange
import randomUtil.table.WeightStatus.*

/**
 * A [RandomTable] that is also weighted.
 * [items] and [weights] must be the same length.
 * [weights] cannot contain more than 1 of the same weight.
 * The item at index X will correspond to the weight at index X (the items are sorted accordingly with the weights that are sorted in ascending order)
 *
 * @constructor Creates a new [WeightedTable]
 * @property [items] The items that will be picked from
 * @property [weights] The weights sorted in ascending order
 * @param [seed] The seed to initialize the [WeightedTable] with
 * @property [weightModifier] The formula used to calculate the weight with a given modifier
 * @property [weightRange] The range of weights that can be picked
 */
open class WeightedTable<T>(
    items: List<T>, weights: IntArray, seed: Long? = null,
    inline val weightModifier: (Int) -> Double = { 1.0 / (1 + (it / 100.0)) }
): RandomTable<T>(items.sortedByOtherArray(weights), seed) {

    protected val weights: IntArray = weights.sortedArray()
    val weightValues: List<Int> = weights.toList()
    protected val weightRange: RandomLongRange

    /**
     * Assesses the status of the weights [IntArray] against the items [other].
     * This is to check that they are the same size, there are no duplicate weights and there are no negative weights.
     *
     * @param [other] The items to check the weights against
     * @return [WeightStatus]
     */
    protected fun IntArray.assessWeightsAgainst(other: List<T>): WeightStatus {
        if (this === weights) {
            return ValidWeights
        }
        if (this.size != other.size) {
            return InvalidLength
        }
        for ((index, weight) in this.withIndex()) {
            if (this.count { it == weight } > 1) {
                return DuplicateWeight(indexOfFirst { it == weight }, indexOfLast { it == weight } )
            } else if (weight < 1) {
                return InvalidWeight(index)
            }
        }
        return ValidWeights
    }

    /**
     * Gets the sum of an [IntArray] as [Long]
     *
     * @return [Long]
     */
    protected fun IntArray.longSum(): Long {
        var sum: Long = 0
        for (value in this) sum += value
        return sum
    }

    init {
        weights.assessWeightsAgainst(this.items).throwWhenBad()
        this.weightRange = RandomLongRange(1L, weights.longSum(), true, initialSeed)
    }

    /**
     * Gets the index of the first [item] in [items]
     *
     * @param [item] The item to get the index of
     * @return [Int]
     */
    fun indexOf(item: T): Int = items.indexOfFirst { it == item }

    /**
     * Gets the weight of the first [item] in [items]
     *
     * @param [item] The item to get the weight of
     * @return [Int]
     */
    fun weightOf(item: T): Int = weights[indexOf(item)]

    /**
     * Gets the default (no custom weights) chance for [item] to be picked (or 0.0 if [item] is not contained)
     *
     * @oaram[item] The item to get the chance of
     * @return [Double]
     */
    fun chanceOf(item: T): Double =
        if (item in this)
            (this.weights[this.indexOf(item)].toDouble() * 100) / this.weightRange.maximum
        else
            0.0

    /**Supplementary functions for extensibility*/
    protected open fun onOtherWeights(otherWeights: IntArray): Boolean {
        otherWeights.assessWeightsAgainst(items).doWhenInvalid { return false }
        return true
    }
    /**Supplementary functions for extensibility*/
    protected open fun getWeightRangeFor(otherWeights: IntArray): RandomLongRange = RandomLongRange(1L, otherWeights.longSum(), true)
    /**Supplementary functions for extensibility*/
    protected open fun getItemsFor(otherWeights: IntArray): List<T> =
        if (otherWeights === weights)
            items
        else
            items.sortedByOtherArray(otherWeights, true)
    /**Supplementary functions for extensibility*/
    protected open fun getWeightsFor(otherWeights: IntArray): IntArray = otherWeights.copyOf()

    private fun itemsWeightRangeWeights(weightsToGet: IntArray): Triple<List<T>, RandomLongRange, IntArray> {
        return if (weightsToGet !== weights && !onOtherWeights(weightsToGet)) {
            Triple(getItemsFor(weightsToGet), getWeightRangeFor(weightsToGet), getWeightsFor(weightsToGet))
        } else {
            Triple(items, weightRange, weights)
        }
    }

    /**
     * Protected function that is a micro optimization to bypass having to check the items and weights every iteration of [nextValue]
     *
     * @param [modifier] The modifier that changes the initial weight value
     * @param [withItems] The items to pick from
     * @param [withWeights] The weights to use when picking the items
     * @param [withWeightRange] The [RandomLongRange] to use when picking the weight
     * @return [T]
     */
    protected fun verifiedNextValue(
        modifier: Int,
        withItems: List<T>,
        withWeights: IntArray,
        withWeightRange: RandomLongRange
    ): T {
        var pickedWeight = withWeightRange.nextValue() * weightModifier(modifier)
        for ((weightIndex, weightValue) in withWeights.withIndex()) {
            pickedWeight -= weightValue
            if (pickedWeight <= 0) {
                return withItems[weightIndex]
            }
        }
        return withItems.last()
    }

    /**
     * Gets the next value with the given weights [withWeights]
     *
     * @param [modifier] The modifier that changes the initial weight value
     * @param [withWeights] The weights to use when picking the items
     * @return[ T]
     */
    fun nextValue(
        modifier: Int,
        withWeights: IntArray
    ): T {
        val (itemsToUse, weightRangeToUse, weightsToUse) = itemsWeightRangeWeights(withWeights)
        return verifiedNextValue(modifier, itemsToUse, weightsToUse, weightRangeToUse)
    }

    /**
     * Gets the next value with the default wieghts [weights]
     *
     * @param [modifier] The modifier that changes the initial weight value
     * @return [T]
     */
    override fun nextValue(modifier: Int): T =
        nextValue(modifier, weights)

    /**
     * Returns a [Map] of [verifiedNextValue] returns with the given [modifier] and given weights [withWeights]
     *
     * @param [amount] The amount of times to call [verifiedNextValue]
     * @param [modifier] The modifier that changes the initial weight value
     * @param [withWeights] The weights to use when picking the items
     * @return [Map]<[T], [Int]>
     */
    fun nextValueMap(
        amount: Int, modifier: Int,
        withWeights: IntArray
    ): Map<T, Int> {
        val (itemsToUse, weightRangeToUse, weightsToUse) = itemsWeightRangeWeights(withWeights)
        val map = HashMap<T, Int>()
        for (run in 0 until amount) {
            map.increment(verifiedNextValue(modifier, itemsToUse, weightsToUse, weightRangeToUse))
        }
        return map
    }

    /**
     * Returns a [Map] of [verifiedNextValue] returns with the given [modifier] and default weights [weights]
     *
     * @param [amount] The amount of times to call [verifiedNextValue]
     * @param [modifier] The modifier that changes the initial weight value
     * @return [Map]<[T], [Int]>
     */
    override fun nextValueMap(amount: Int, modifier: Int): Map<T, Int> =
        nextValueMap(amount, modifier, weights)

    /**
     * Returns a [List] of size [amount] full of [verifiedNextValue] returns with the given [modifier] and given weights [withWeights]
     *
     * @param [amount] The size of the return [List]
     * @param [modifier] The modifier that changes the initial weight value
     * @param [withWeights] The weights to use when picking the items
     * @return [List]<[T]>
     */
    fun nextValueList(
        amount: Int, modifier: Int,
        withWeights: IntArray
    ): List<T> {
        val (itemsToUse, weightRangeToUse, weightsToUse) = itemsWeightRangeWeights(withWeights)
        return List(amount) { verifiedNextValue(modifier, itemsToUse, weightsToUse, weightRangeToUse) }
    }

    /**
     * Returns a [List] of size [amount] full of [verifiedNextValue] return values with the given [modifier] and default weights [weights]
     *
     * @param [amount] The size of the return [List]
     * @param [modifier] The modifier that changes the initial weight value
     * @return [List]<[T]>
     */
    override fun nextValueList(amount: Int, modifier: Int): List<T> =
        nextValueList(amount, modifier, weights)

    /**
     * Returns a [MemoizedWeightedTable]<[T]> out of [items], [weights], [initialSeed] and [weightModifier]
     *
     * @return [MemoizedWeightedTable]<[T]>
     */
    fun toMemoizedWeightedTable(): MemoizedWeightedTable<T> = MemoizedWeightedTable(items = items, weights = weights, seed = initialSeed, weightModifier = weightModifier)
}
