package randomUtil.table

/**
 * Sorts the given [List] by the order of [other]. Will sort [other] in place if [sortOther] is true
 *
 * @param [other] The [IntArray] to sort the [List] by
 * @param [sortOther] Whether or not to sort [other] in place
 * @return [List]<[T]>
 */
internal fun <T> List<T>.sortedByOtherArray(other: IntArray, sortOther: Boolean=false): List<T> {
    if (this.size != other.size) return this
    return zip(other.toList())
        .sortedBy { (_, index) -> index }
        .map { it.first }
        .also { if (sortOther) other.sort() }
}

/**
 * Returns the indices of [this] but sorted by [other]
 *
 * @param [other] The [IntArray] to sort the indices by
 * @return [IntArray]
 */
internal fun <T> List<T>.sortedIndicesByOtherArray(other: IntArray): IntArray =
    if (this.size != other.size)
        IntArray(size) { it }
    else
        IntArray(size) { it }.sortedBy { other[it] }.toIntArray()

/**
 * Creates a [ListRemap]<[T]> based on [items] and [sortedBy] where each item in [items] index `x` corresponds to the number at [sortedBy] index `x`
 *
 * @param [items] The [List] to remap
 * @param [sortedBy] The [IntArray] to sort [items] by
 * @return [ListRemap]<[T]>
 */
internal fun <T> sortedListRemap(items: List<T>, sortedBy: IntArray): ListRemap<T> =
        ListRemap(items, items.sortedIndicesByOtherArray(sortedBy))
