package randomUtil

internal fun <T> HashMap<T, Int>.increment(item: T) {
    if (item !in this) this[item] = 0
    this[item] = (this[item] as Int) + 1
}