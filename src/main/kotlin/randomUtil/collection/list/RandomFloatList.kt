package randomUtil.collection.list

import kotlin.random.Random

/**
 * Generates a [List]<[Float]> of size [size] with elements ranging from [min] to [max] (non inclusive)
 *
 * @param [size] The size of the [List]<[Float]> being generated
 * @param [min] The minimum value being generated
 * @param [max] The maximum value being generated
 * @param [seed] The seed to use for the random generator
 * @param [allowDuplicates] Whether or not to allow duplicate entries
 */
@JvmOverloads fun randomFloatList(
    size: Int, min: Double, max: Double,
    seed: Long = System.currentTimeMillis(),
    allowDuplicates: Boolean = true): List<Float> {
    if (min !in Float.MIN_VALUE .. Float.MAX_VALUE) {
        throw Exception("min ($min) must be in range ${Float.MIN_VALUE .. Float.MAX_VALUE}")
    }
    // So you can generate the whole range
    if (max !in Float.MIN_VALUE .. Float.MAX_VALUE) {
        throw Exception("max ($max) must be in range ${Float.MIN_VALUE .. Float.MAX_VALUE}")
    }
    val random = Random(seed)
    if (allowDuplicates) {
        return List(size) { random.nextDouble(min, max).toFloat() }
    }
    val returnList = mutableListOf<Float>()
    var amountAdded = 0
    while (amountAdded != size) {
        val nextNumber = random.nextDouble(min, max).toFloat()
        if (nextNumber !in returnList) {
            returnList.add(nextNumber)
            amountAdded++
        }
    }
    return returnList.toList()
}
