package randomUtil.collection.array

import kotlin.math.abs
import kotlin.random.Random

/**
 * Generates a [ByteArray] of size [size] with elements ranging from [min] to [max] (non inclusive)
 * If you decide not to [allowDuplicates], [size] must be a maximum of [abs] ( [min] - [max] ) because you can't generate, say 10 unique integral values between 1 and 7
 *
 * @param [size] The size of the [ByteArray] being generated
 * @param [min] The minimum value being generated
 * @param [max] The maximum value being generated
 * @param [seed] The seed to use for the random generator
 * @param [allowDuplicates] Whether or not to allow duplicate entries
 */
@JvmOverloads fun randomByteArray(
    size: Int, min: Int, max: Int,
    seed: Long = System.currentTimeMillis(),
    allowDuplicates: Boolean = true): ByteArray {
    if (min !in Byte.MIN_VALUE .. Byte.MAX_VALUE) {
        throw Exception("min ($min) must be in range ${Byte.MIN_VALUE .. Byte.MAX_VALUE}")
    }
    // So you can generate the whole range
    if (max !in Byte.MIN_VALUE .. Byte.MAX_VALUE+1) {
        throw Exception("max ($max) must be in range ${Byte.MIN_VALUE .. Byte.MAX_VALUE+1}")
    }
    val random = Random(seed)
    if (allowDuplicates) {
        return ByteArray(size) { random.nextInt(min, max).toByte() }
    }
    val distance = abs(min - max)
    if (distance < size) {
        throw Exception("Cannot create $size unique values in a range of $distance numbers")
    }
    val returnList = mutableListOf<Byte>()
    var amountAdded = 0
    while (amountAdded != size) {
        val nextNumber = random.nextInt(min, max).toByte()
        if (nextNumber !in returnList) {
            returnList.add(nextNumber)
            amountAdded++
        }
    }
    return returnList.toByteArray()
}
