package randomUtil.collection.array

import kotlin.math.abs
import kotlin.random.Random

/**
 * Generates a [LongArray] of size [size] with elements ranging from [min] to [max] (non inclusive)
 * If you decide not to [allowDuplicates], [size] must be a maximum of [abs] ( [min] - [max] ) because you can't generate, say 10 unique integral values between 1 and 7
 *
 * @param [size] The size of the [LongArray] being generated
 * @param [min] The minimum value being generated
 * @param [max] The maximum value being generated
 * @param [seed] The seed to use for the random generator
 * @param [allowDuplicates] Whether or not to allow duplicate entries
 */
@JvmOverloads fun randomLongArray(
    size: Int, min: Long, max: Long,
    seed: Long = System.currentTimeMillis(),
    allowDuplicates: Boolean = true): LongArray {
    val random = Random(seed)
    if (allowDuplicates) {
        return LongArray(size) { random.nextLong(min, max) }
    }
    val distance = (min.toBigInteger() - max.toBigInteger()).abs()
    if (distance < size.toBigInteger()) {
        throw Exception("Cannot create $size unique values in a range of $distance numbers")
    }
    val returnList = mutableListOf<Long>()
    var amountAdded = 0
    while (amountAdded != size) {
        val nextNumber = random.nextLong(min, max)
        if (nextNumber !in returnList) {
            returnList.add(nextNumber)
            amountAdded++
        }
    }
    return returnList.toLongArray()
}
