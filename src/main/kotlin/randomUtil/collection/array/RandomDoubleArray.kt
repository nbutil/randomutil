package randomUtil.collection.array

import kotlin.random.Random

/**
 * Generates a [DoubleArray] of size [size] with elements ranging from [min] to [max] (non inclusive)
 *
 * @param [size] The size of the [DoubleArray] being generated
 * @param [min] The minimum value being generated
 * @param [max] The maximum value being generated
 * @param [seed] The seed to use for the random generator
 * @param [allowDuplicates] Whether or not to allow duplicate entries
 */
@JvmOverloads fun randomDoubleArray(
    size: Int, min: Double, max: Double,
    seed: Long = System.currentTimeMillis(),
    allowDuplicates: Boolean = true): DoubleArray {
    if (min !in Double.MIN_VALUE .. Double.MAX_VALUE) {
        throw Exception("min ($min) must be in range ${Double.MIN_VALUE .. Double.MAX_VALUE}")
    }
    // So you can generate the whole range
    if (max !in Double.MIN_VALUE .. Double.MAX_VALUE) {
        throw Exception("max ($max) must be in range ${Double.MIN_VALUE .. Double.MAX_VALUE}")
    }
    val random = Random(seed)
    if (allowDuplicates) {
        return DoubleArray(size) { random.nextDouble(min, max) }
    }
    val returnList = mutableListOf<Double>()
    var amountAdded = 0
    while (amountAdded != size) {
        val nextNumber = random.nextDouble(min, max)
        if (nextNumber !in returnList) {
            returnList.add(nextNumber)
            amountAdded++
        }
    }
    return returnList.toDoubleArray()
}
