package randomUtil.collection.array

import kotlin.random.Random

/**
 * Generates a [FloatArray] of size [size] with elements ranging from [min] to [max] (non inclusive)
 *
 * @param [size] The size of the [FloatArray] being generated
 * @param [min] The minimum value being generated
 * @param [max] The maximum value being generated
 * @param [seed] The seed to use for the random generator
 * @param [allowDuplicates] Whether or not to allow duplicate entries
 */
@JvmOverloads fun randomFloatArray(
    size: Int, min: Double, max: Double,
    seed: Long = System.currentTimeMillis(),
    allowDuplicates: Boolean = true): FloatArray {
    if (min !in Float.MIN_VALUE .. Float.MAX_VALUE) {
        throw Exception("min ($min) must be in range ${Float.MIN_VALUE .. Float.MAX_VALUE}")
    }
    // So you can generate the whole range
    if (max !in Float.MIN_VALUE .. Float.MAX_VALUE) {
        throw Exception("max ($max) must be in range ${Float.MIN_VALUE .. Float.MAX_VALUE}")
    }
    val random = Random(seed)
    if (allowDuplicates) {
        return FloatArray(size) { random.nextDouble(min, max).toFloat() }
    }
    val returnList = mutableListOf<Float>()
    var amountAdded = 0
    while (amountAdded != size) {
        val nextNumber = random.nextDouble(min, max).toFloat()
        if (nextNumber !in returnList) {
            returnList.add(nextNumber)
            amountAdded++
        }
    }
    return returnList.toFloatArray()
}
