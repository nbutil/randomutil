package randomUtil.collection.array

import kotlin.math.abs
import kotlin.random.Random

/**
 * Generates a [IntArray] of size [size] with elements ranging from [min] to [max] (non inclusive)
 * If you decide not to [allowDuplicates], [size] must be a maximum of [abs] ( [min] - [max] ) because you can't generate, say 10 unique integral values between 1 and 7
 *
 * @param [size] The size of the [IntArray] being generated
 * @param [min] The minimum value being generated
 * @param [max] The maximum value being generated
 * @param [seed] The seed to use for the random generator
 * @param [allowDuplicates] Whether or not to allow duplicate entries
 */
@JvmOverloads fun randomIntArray(
    size: Int, min: Int, max: Int,
    seed: Long = System.currentTimeMillis(),
    allowDuplicates: Boolean = true): IntArray {
    if (min !in Int.MIN_VALUE .. Int.MAX_VALUE) {
        throw Exception("min ($min) must be in range ${Int.MIN_VALUE .. Int.MAX_VALUE}")
    }
    // So you can generate the whole range
    if (max !in Int.MIN_VALUE .. Int.MAX_VALUE.toLong()+1) {
        throw Exception("max ($max) must be in range ${Int.MIN_VALUE .. Int.MAX_VALUE.toLong()+1}")
    }
    val random = Random(seed)
    if (allowDuplicates) {
        return IntArray(size) { random.nextLong(min.toLong(), max.toLong()).toInt() }
    }
    val distance = abs(min.toLong() - max.toLong())
    if (distance < size) {
        throw Exception("Cannot create $size unique values in a range of $distance numbers")
    }
    val returnList = mutableListOf<Int>()
    var amountAdded = 0
    while (amountAdded != size) {
        val nextNumber = random.nextLong(min.toLong(), max.toLong()).toInt()
        if (nextNumber !in returnList) {
            returnList.add(nextNumber)
            amountAdded++
        }
    }
    return returnList.toIntArray()
}
